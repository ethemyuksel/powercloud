package com.bayzdelivery.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.bayzdelivery.model.Person;
import com.bayzdelivery.repositories.PersonRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PersonControllerTest {

  MockMvc mockMvc;

  @Mock
  private PersonController personController;
  
  @Autowired
  private WebApplicationContext context;

  @Value("${local.server.port}")
  private int port;
  
  @Autowired
  private TestRestTemplate template;

  @Autowired
  PersonRepository personRepository;
  
  private String url = null;

  @Before
  public void setup() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(personController).build();
    url = "http://localhost:" + port; 
  }

  
  @Test
  public void testUserShouldBeRegistered() throws Exception {
	  Person person = new Person();
	  person.setName("Murat Kilis");
	  person.setEmail("mkilis@gmail.com");
	  person.setRegistrationNumber("151515");
	  Person returnPer = template.postForEntity(url + "/api/api/person", person, Person.class).getBody();
	  assertNotNull(returnPer); 
	  assertNotNull(returnPer.getId());	   
  }
  
  @Test
  public void testGetAllPersons() throws Exception {
	  Person[] perArr = template.getForEntity(url + "api/api/person",Person[].class).getBody();
	  List<Person> perList = Arrays.asList(perArr);
	  assertTrue(!perList.isEmpty());
  }
  
  @Test
  public void testGetPersonById() throws Exception {
	  Person per = template.getForEntity(url + "api/api/person/1",Person.class).getBody();
	  assertNotNull(per);
  }
  
  @Test
  public void testGetPersonByRegNumber() throws Exception {
	  Person per = template.getForEntity(url + "api/api/personByRegNr/123123",Person.class).getBody();
	  assertNotNull(per);
  }
  
  
  
  

}
