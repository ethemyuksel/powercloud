package com.bayzdelivery.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.bayzdelivery.exceptions.DeliveryTimeAlreadyExists;
import com.bayzdelivery.jobs.DelayedDeliveryNotifier;
import com.bayzdelivery.model.Delivery;
import com.bayzdelivery.model.DeliveryManComission;
import com.bayzdelivery.model.Person;
import com.bayzdelivery.repositories.PersonRepository;
import com.bayzdelivery.service.DeliveryService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DeliveryControllerTest {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryControllerTest.class);
	
	MockMvc mockMvc;

	@Mock
	private DeliveryController deliveryController;

	@Value("${local.server.port}")
	private int port;

	@Autowired
	private TestRestTemplate template;

	@Autowired
	PersonRepository personRepository;

	@Autowired
	private DeliveryService deliveryService;

	private String url = null;

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(deliveryController).build();
		url = "http://localhost:" + port;
	}

	@Test
	public void testCreateNewDelivery() throws Exception {
		Delivery deliver = new Delivery();
		Person person = new Person();
		person.setId(1L);
		deliver.setCustomer(person);
		Person deliverMan = new Person();
		deliverMan.setId(19L);
		deliver.setDeliveryMan(deliverMan);
		deliver.setDistance(14L);
		deliver.setStartTime(Instant.parse("2023-04-22T15:00:00Z"));
		deliver.setEndTime(Instant.parse("2023-04-22T16:30:00Z"));
		deliver.setPrice(14000L);
		Delivery returnDel = template.postForEntity(url + "/api/delivery", deliver, Delivery.class).getBody();
		assertNotNull(returnDel);
		assertNotNull(returnDel.getId());
		assertEquals((long) returnDel.getComission(),
				(long) (deliver.getPrice() * 0.05 + deliver.getDistance() * 0.05));
	}

	@Test
  public void testTop3DeliveryMan() {
	  DeliveryManComission[] returnDel = template.getForEntity(url + "/api/delivery/top3deliveryman/2023-04-01T16:00:00Z/2023-06-22T16:00:00Z", DeliveryManComission[].class).getBody();
	  for(DeliveryManComission dmc : returnDel) {
		  LOG.debug(dmc.getName() + " ==== " + dmc.getAvg());
	  }
  }

	@Test
	public void testTop3DeliveryManFromService() {
		Instant startTime = Instant.parse("2023-01-01T16:00:00Z");
		Instant endtime = Instant.parse("2023-06-01T17:00:00Z");
		List<DeliveryManComission> list = deliveryService.getTop3DeliveryMan(startTime, endtime);
		for (DeliveryManComission dmc : list) {
			LOG.debug(dmc.getName() + " : " + dmc.getAvg());
		}
	}

}
