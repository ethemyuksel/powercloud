package com.bayzdelivery;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bayzdelivery.model.Person;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HttpConnectionTest {

	private static final Logger LOG = LoggerFactory.getLogger(HttpConnectionTest.class);

	public static void main(String[] args) throws Exception {
		new HttpConnectionTest().saveNewPerson();

	}

	public void saveNewPerson() throws Exception {
		Person person = new Person();
		person.setName("Murat Kilis");
		person.setEmail("mkilis@gmail.com");
		person.setRegistrationNumber("151515");
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(person);
		LOG.debug(jsonStr);
		
		String urlStr = "http://localhost:8081/api/api/person";
		URL url = new URL(urlStr);
		HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
		urlConn.setDoOutput(true);
		urlConn.setDoInput(true);
		urlConn.setRequestMethod("POST");
		urlConn.setRequestProperty("Content-Type", "application/json");
		OutputStream outStrm = urlConn.getOutputStream();
		outStrm.write(jsonStr.getBytes());
		try {
			InputStream inStrm = urlConn.getInputStream();
			logInputStream(inStrm);
		} catch(Exception e) {
			logInputStream(urlConn.getErrorStream());
		}
	}
	
	public void testTop3DeliveryMan() throws Exception {
		String urlStr = "http://localhost:8081/api/delivery/top3deliveryman/2023-04-01T16:00:00Z/2023-06-22T16:00:00Z";
		URL url = new URL(urlStr);
		HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
		urlConn.setDoOutput(true);
		urlConn.setDoInput(true);
		urlConn.setRequestMethod("GET");
		InputStream inStr = urlConn.getInputStream();
		byte[] readBytes = new byte[1024];
		int readBytesCount = 0;
		String str = "";
		while ((readBytesCount = inStr.read(readBytes)) != -1) {
			str += new String(readBytes, 0, readBytesCount);
		}

		LOG.debug(str);
	}
	
	public void logInputStream(InputStream inStr) throws Exception {
		byte[] readBytes = new byte[1024];
		int readBytesCount = 0;
		String str = "";
		while ((readBytesCount = inStr.read(readBytes)) != -1) {
			str += new String(readBytes, 0, readBytesCount);
		}

		LOG.debug(str);
	}
	

}
