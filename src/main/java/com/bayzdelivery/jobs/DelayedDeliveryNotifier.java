package com.bayzdelivery.jobs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bayzdelivery.model.Delivery;
import com.bayzdelivery.service.DeliveryService;

@Component
@EnableScheduling
@EnableAsync
public class DelayedDeliveryNotifier {

    private static final Logger LOG = LoggerFactory.getLogger(DelayedDeliveryNotifier.class);
    private static int TIME_INTERVAL = 45;
    @Autowired
    DeliveryService deliverService;
    /**
     *  Use this method for the TASK 3
     */
    @Scheduled(fixedDelay = 30000)
    public void checkDelayedDeliveries() {
    	List<Delivery> list = deliverService.getNotDeliveredIn45Mins(TIME_INTERVAL);

    	if(!list.isEmpty()) {
    		notifyCustomerSupport();
    	}
    	
    }

    /**
     * This method should be called to notify customer support team
     * It just writes notification on console but it may be email or push notification in real.
     * So that this method should run in an async way.
     */
    /**
     * comment by ethem: Async annotation does not make this method async. For real asynch we need to process
     * notification within thread
     */
    public void notifyCustomerSupport() {
    	Runnable r = () -> {
    		LOG.info("Customer support team is notified!");
    	};
        new Thread(r).start();
    }
}
