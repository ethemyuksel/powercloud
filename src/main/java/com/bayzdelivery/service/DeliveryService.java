package com.bayzdelivery.service;

import java.time.Instant;
import java.util.List;

import com.bayzdelivery.exceptions.DeliveryTimeAlreadyExists;
import com.bayzdelivery.model.Delivery;
import com.bayzdelivery.model.DeliveryManComission;

public interface DeliveryService {

  public Delivery save(Delivery delivery) throws DeliveryTimeAlreadyExists;

  public Delivery findById(Long deliveryId);
  
  public Delivery findByStartEndTime(Instant startTime, Instant endTime, long deliverManId);
  
  public List<Delivery> getNotDeliveredIn45Mins(int min);
  
  public List<DeliveryManComission>  getTop3DeliveryMan(Instant startTime, Instant endTime);
  
  
}
