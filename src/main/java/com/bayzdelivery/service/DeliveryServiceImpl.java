package com.bayzdelivery.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bayzdelivery.exceptions.DeliveryTimeAlreadyExists;
import com.bayzdelivery.model.Delivery;
import com.bayzdelivery.model.DeliveryManComission;
import com.bayzdelivery.repositories.DeliveryRepository;

@Service
public class DeliveryServiceImpl implements DeliveryService {

	@Autowired
	DeliveryRepository deliveryRepository;

	public Delivery save(Delivery delivery) throws DeliveryTimeAlreadyExists {
		Delivery checkDelivery = findByStartEndTime(delivery.getStartTime(), delivery.getEndTime(), delivery.getDeliveryMan().getId());
		if(checkDelivery != null) {
			throw new DeliveryTimeAlreadyExists();
		}
		long comision = (long) (delivery.getPrice() * 0.05 + delivery.getDistance() * 0.05);
		delivery.setComission(comision);
		return deliveryRepository.save(delivery);
	}

	public Delivery findById(Long deliveryId) {
		Optional<Delivery> optionalDelivery = deliveryRepository.findById(deliveryId);
		if (optionalDelivery.isPresent()) {
			return optionalDelivery.get();
		} else
			return null;
	}
	
	public Delivery findByStartEndTime(Instant startTime, Instant endTime, long deliveryManId) {
		Optional<Delivery> optionalDelivery = deliveryRepository.getDeliverByStartEndTime(startTime, endTime, deliveryManId);
		return optionalDelivery.orElse(null);
	}
	
	public List<Delivery> getNotDeliveredIn45Mins(int min) {
		return deliveryRepository.getNotDeliveredIn45Mins(min);
	}
	
	public List<DeliveryManComission> getTop3DeliveryMan(Instant startTime, Instant endTime) {
		List<Object[]> list = deliveryRepository.getTop3DeliveryMan(startTime, endTime);
		List<DeliveryManComission> retList = list
			.stream()
			.map(o -> {
				DeliveryManComission dmc = new DeliveryManComission();
				dmc.setComission((long)Double.parseDouble(o[0].toString()));
				dmc.setAvg((long)Double.parseDouble(o[1].toString()));
				dmc.setName(o[2].toString());
				return dmc;
			})
			.limit(3)
			.collect(Collectors.toList());
		
		return retList;
		
	}

}
