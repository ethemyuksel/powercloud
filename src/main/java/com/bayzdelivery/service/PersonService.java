package com.bayzdelivery.service;

import java.util.List;

import com.bayzdelivery.exceptions.UserAlreadyDefinedException;
import com.bayzdelivery.model.Person;

public interface PersonService {
	public List<Person> getAll();

	public Person save(Person p) throws UserAlreadyDefinedException;

	public Person findById(Long personId);

	Person findByRegistrationNumber(String regNumber);

}
