package com.bayzdelivery.repositories;

import com.bayzdelivery.model.Delivery;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface DeliveryRepository extends CrudRepository<Delivery, Long> {
	@Query("SELECT d FROM Delivery d WHERE ((d.startTime <= ?1 and d.endTime >= ?1) OR (d.startTime <= ?2 and d.endTime >= ?2)) AND d.deliveryMan.id = ?3 ") 
	Optional<Delivery> getDeliverByStartEndTime(Instant startTime, Instant endTime, long deliveryManId);
	
	@Query(value = "with tempTable as(\n"
			+ "SELECT (extract(epoch from d.end_time)- extract(epoch from d.start_time)) / 60 AS minutes,*\n"
			+ "from delivery d )\n"
			+ "select *\n"
			+ "from tempTable where tempTable.minutes > ?1", nativeQuery = true)
	List<Delivery> getNotDeliveredIn45Mins(int min);
	
	@Query("SELECT SUM(comission) as sumx ,AVG(comission) as average,d.deliveryMan.name "
			+"from Delivery d where d.startTime >= ?1 and d.endTime <= ?2 "
			+"group by d.deliveryMan,d.deliveryMan.name order by sumx desc")
	List<Object[]> getTop3DeliveryMan(Instant startTime, Instant endTime);
	

}
