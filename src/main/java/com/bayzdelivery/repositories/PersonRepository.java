package com.bayzdelivery.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import com.bayzdelivery.model.Person;

@RestResource(exported=false)
public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {
	@Query("SELECT p FROM Person p WHERE p.registrationNumber = ?1")
	Optional<Person> findByRegistrationNumber(String registrationNumber);
}
