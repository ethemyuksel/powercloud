package com.bayzdelivery.model;

import java.io.Serializable;

public class DeliveryManComission implements Serializable {
	private static final long serialVersionUID = 8959525855549090069L;
	private String name;
	private long comission;
	private long avg;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getComission() {
		return comission;
	}
	public void setComission(long comission) {
		this.comission = comission;
	}
	public long getAvg() {
		return avg;
	}
	public void setAvg(long avg) {
		this.avg = avg;
	}
}
