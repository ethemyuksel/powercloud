package com.bayzdelivery.exceptions;

public class DeliveryTimeAlreadyExists extends Exception implements BayzDeliveryException {
	public DeliveryTimeAlreadyExists() {
		super("Delivery time already exists in DB");
	}
}
