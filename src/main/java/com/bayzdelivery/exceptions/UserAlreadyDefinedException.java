package com.bayzdelivery.exceptions;

public class UserAlreadyDefinedException extends Exception implements BayzDeliveryException {
	public UserAlreadyDefinedException() {
		super("User with this Registration Number is already defined");
	}
}
